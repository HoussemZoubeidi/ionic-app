import {Component} from '@angular/core';
import {IonicPage, NavParams} from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";
import {LocalNotifications} from '@ionic-native/local-notifications';
import {NavController} from "ionic-angular";
import {HomePage} from "../home/home";

/**
 * Generated class for the WelcomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {

  username: any;
  id: any;

  constructor(public api: ApiProvider, localnotification: LocalNotifications, public navCtrl: NavController) {
    this.id = localStorage.getItem("id");

    this.getusernameid()
    //****//

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WelcomePage');
  }

  getusernameid() {
    this.api.getUserid(this.id).then(data => {
      this.username = JSON.parse(JSON.stringify(data)).username
      console.log('this is ' + (JSON.stringify(data)))
      console.log("brought: " + this.username)
    })
  }

  /*  Logout() {
      this.username = "";
      this.navCtrl.push(HomePage)
    }*/


}


